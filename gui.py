import secrets
import engine as e
import tkinter as tk
from pathlib import Path
import random, pathlib, json
from functools import partial
from tkinterhtml import HtmlFrame
from tkinter import messagebox as msg


class Main(tk.Frame):
    def __init__(self, parent):

        self.parent = parent
        self.parent.resizable(False, False)
        tk.Frame.__init__(self, self.parent)

        self.top_frame = tk.Frame(self)

        self.hadith_frame = tk.Frame(self.top_frame)

        self.hadith_tb = HtmlFrame(self.hadith_frame, relief="flat")
        self.hadith_tb.pack(side="top", pady=10)
        self.random_hadith_btn = tk.Button(
            self.hadith_frame,
            text="Random Hadith",
            font=("FreeMono", 16),
            command=self.get_hadith,
        )
        self.hadith_frame.pack(side="left", padx=50, pady=10)

        # Frame where user inputs journal stuff
        self.entry_frame = tk.Frame(self.top_frame)
        self.label1 = tk.Label(
            self.entry_frame,
            relief="solid",
            text="Hadith Finder & Journal",
            font=("FreeMono", 20, "bold"),
        ).pack(side="top", pady=20, padx=20)
        self.entry_tb = tk.Text(self.entry_frame, width=60, height=30)
        self.entry_tb.pack(side="top", pady=20)
        self.save_button = tk.Button(
            self.entry_frame, text="Save", font=("FreeMono", 16), command=self.save
        )
        self.save_button.pack(side="bottom", pady=10)

        self.save_button = tk.Button(
            self.entry_frame, text="Open", font=("FreeMono", 16), command=self.fetch
        )
        self.save_button.pack(side="bottom")

        self.entry_frame.pack(side="right", padx=50, pady=10)

        self.add_button = tk.Button(
            self.hadith_frame,
            text="Add Hadith",
            font=("FreeMono", 16),
            command=self.add_hadith,
        )
        self.add_button.pack(side="bottom")
        self.random_hadith_btn.pack(side="bottom", pady=10)

        self.top_frame.pack(side="top")

        self.hadith_dict = {}

    def fetch(self):
        pass
    
    def insert(self, content):
        """Inserts content into the hadith textbox"""
        self.saved_data = content
        self.hadith_tb.set_content(f"{content}")

    def add_hadith(self):

        root = tk.Tk()
        Add_Hadith(root, self).pack()

    def get_hadith(self):
        """Get random hadith and insert into hadith tb"""

        self.hadith_dict = e.return_random()
        self.hadith_eng = self.hadith_dict["hadith"][0]
        self.eng_title = self.hadith_eng["chapterTitle"]
        self.eng_body = self.hadith_eng["body"]

        self.hadith_tb.set_content(
            f"<h2>{self.eng_title}\n{self.eng_body}\n</h1>"
        )

    def save(self):

        pass
        self.entry = self.entry_tb.get("1.0", "end-1c")

        with open('data.json', 'x') as db:
            data =              


        if self.hadith_dict:
            self.path = self.hadith_dict["hadithNumber"]
            with open(f"entries/{self.path}.txt", "a") as db:

                # dict_to_write = {"hadith": self.hadith_dict, "entry": self.entry}
                db.write(json.dumps(dict_to_write))
        else:
            with open(f"entries/{secrets.token_hex(8)}.txt", "x") as db:
                json.dump({"hadith": self.saved_data, "entry": self.entry}, db)

        msg.showinfo("Saved", "Content Saved Successfully")


class Add_Hadith(tk.Frame):
    """Class for the popup window for adding text to the hadith textbox"""
    def __init__(self, parent, main):
        self.parent = parent
        tk.Frame.__init__(self, self.parent)

        self.add_frame = tk.Frame(self)
        self.add_tb = tk.Text(self.add_frame, width=60, height=30)
        self.add_tb.pack(side="top", pady=10)
        self.add_button_2 = tk.Button(
            self.add_frame,
            text="Add Hadith",
            font=("FreeMono", 16),
            command=partial(self.save_hadith, main),
        )
        self.add_button_2.pack(side="bottom")
        self.add_frame.pack(side="bottom")

    def save_hadith(self, main):

        self.content = self.add_tb.get("1.0", "end-1c")
        main.insert(self.content)
        self.parent.destory()


if __name__ == "__main__":

    root = tk.Tk()
    main = Main(root).pack()
    tk.mainloop()
