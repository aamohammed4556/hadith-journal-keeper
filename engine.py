import requests, pprint, json

headers = {"x-api-key": "SqD712P3E82xnwOAEOkGd5JZH8s9wRR24TqNFzjk"}


def return_collec():

    global headers

    url = "https://api.sunnah.com/v1/collections"
    payload = "{}"

    response = requests.request("GET", url, data=payload, headers=headers)
    response = json.loads(response.text)

    coll_list = []

    for collection in response["data"]:

        coll_list.append(collection)

    return coll_list


def return_books(collection):

    global headers

    url = f"https://api.sunnah.com/v1/collections/{collection}/books"
    payload = "{}"
    response = requests.request("GET", url, data=payload, headers=headers)
    return json.loads(response.text)


def return_chap(collection, book):

    url = f"https://api.sunnah.com/v1/collections/{collection}/books/{book}/chapters"

    payload = "{}"

    response = requests.request("GET", url, data=payload, headers=headers)

    return json.loads(response.text)


def return_hadith(book, collection):

    url = f"https://api.sunnah.com/v1/collections/{collection}/books/{book}/hadiths"

    payload = "{}"
    response = requests.request("GET", url, data=payload, headers=headers)

    print(response.text)

    return json.loads(response.text)


def return_random() -> str:
    """Make request to api.sunnah.com and return hadith"""

    url = "https://api.sunnah.com/v1/hadiths/random"
    payload = "{}"
    response = requests.request("GET", url, data=payload, headers=headers)

    return json.loads(response.text)
